from behave import given, when, then
from hamcrest import assert_that, equal_to


# ***********************************************************
# *********************** Given Steps ***********************
# ***********************************************************


# ***********************************************************
# *********************** When Steps ************************
# ***********************************************************


@when('I click on the "Cargar saldo" button')
def step_impl(context):
    context.page.click_on_cargar_saldo()


@when('I click on the currency')
def step_impl(context):
    context.page.click_on_ars()


@when('I enter the {amount}')
def step_impl(context, amount):
    context.page.set_amount_input(amount)


@when('I click on the "Continuar" button')
def step_impl(context):
    context.page.click_on_continuar()


# ***********************************************************
# *********************** Then Steps ************************
# ***********************************************************


@then('should see a message')
def step_impl(context):
    assert_that(context.page.validate_message(), equal_to(True))
