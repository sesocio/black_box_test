from behave import given, when, then
from hamcrest import assert_that, equal_to, is_

from pages.deposit_amount import DepositAmount
from pages.login_page import LoginPage


# ***********************************************************
# *********************** Given Steps ***********************
# ***********************************************************

@given('I open the web page')
def step_impl(context):
    context.page = LoginPage()
    context.page.open(url=context.config.userdata["url"],
                      browser_name=context.config.userdata["browser_name"],
                      executable_path=context.config.userdata["executable_path"],
                      implicitly_wait=context.config.userdata["implicitly_wait"])


# ***********************************************************
# *********************** When Steps ************************
# ***********************************************************

@when('I click on "Ingresar" button')
def step_impl(context):
    context.page.click_on_ingresar()


@when('I enter email {username}')
def step_impl(context, username):
    context.page.set_email_input(username)


@when('I enter password {password}')
def step_impl(context, password):
    context.page.set_password_input(password)


@when('I click on "Iniciar sesión" button')
def step_impl(context):
    context.page.click_on_login()


@when('I enter on "email" field')
def step_impl(context):
    context.page.enter_on_email()


@when('I enter on "password" field')
def step_impl(context):
    context.page.enter_on_password()


@when('I click on My Account')
def step_impl(context):
    context.page.click_on_mi_cuenta()
    context.page = DepositAmount()


@when('I click on "Registrate" button')
def step_impl(context):
    context.page.click_on_registrate()


@when('I click on "¿Olvidaste tu contraseña?" button')
def step_impl(context):
    context.page.click_on_olvidaste_contrasenia()


@when('I click on "Iniciar sesión con Apple" button')
def step_impl(context):
    context.page.click_on_login_apple()


# ***********************************************************
# *********************** Then Steps ************************
# ***********************************************************


@then('should see the avatar')
def step_impl(context):
    assert_that(context.page.validate_login(), is_(True))


@then('should then see the "Iniciar sesión" button disabled')
def step_impl(context):
    assert_that(context.page.validate_button_login(), is_(False))


@then('should see the {message} in the email field')
def step_impl(context, message):
    assert_that(context.page.validate_message_email(), equal_to(message))


@then('should see the {message} in the password field')
def step_impl(context, message):
    assert_that(context.page.validate_message_password(), equal_to(message))


@then('should see the {message} when the email is incorrect')
def step_impl(context, message):
    assert_that(context.page.validate_message_login(), equal_to(message))


@then('should see the {message} when the password is incorrect')
def step_impl(context, message):
    assert_that(context.page.validate_message_login(), equal_to(message))


@then('should see the {message} when entering the linktext')
def step_impl(context, message):
    assert_that(context.page.validate_message_registrate(), equal_to(message))


@then('should see the {message} when entering the linktext "¿Olvidaste tu contraseña?"')
def step_impl(context, message):
    assert_that(context.page.validate_message_olvidaste_contrasenia(), equal_to(" " + message + " "))


@then('should see the {message} when entering the linktext Login Apple')
def step_impl(context, message):
    assert_that(context.page.validate_message_login_apple(), equal_to(message))


@when('I click on "Iniciar sesión con Google" button')
def step_impl(context):
    context.page.click_on_login_google()


@then('should see the {message} when entering the linktext Login Google')
def step_impl(context, message):
    assert_that(context.page.validate_message_login_google(), equal_to(message))
