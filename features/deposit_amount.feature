Feature: deposit amount

  As a user
  I want to load balance
  To invest in the platform

  @wip
  Scenario Outline: TC-0001 Validate deposit
    Given I open the web page
    When I click on "Ingresar" button
    When I enter email <email>
    And I enter password <password>
    And I click on "Iniciar sesión" button
    #Then should see the avatar
    And I click on My Account
    And I click on the "Cargar saldo" button
    And I click on the currency
    And I enter the <amount>
    And I click on the "Continuar" button
    Then should see a message
    Examples:
      | email              | password | amount |
      | colombia@gmail.com | Test1234 | 0      |