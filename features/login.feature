Feature: login page

  As a user
  I want to login to Sesocio
  So that the user can use the platform

  Background:
    Given I open the web page
    When I click on "Ingresar" button


  Scenario: TC-0001 Validate correct login
    And I enter email pool.meza@gmail.com
    And I enter password Test1234
    And I click on "Iniciar sesión" button
    Then should see the avatar


  Scenario: TC-0002 Validate login "Iniciar sesión" button disabled
    And I enter email pool.meza1@gmail.com
    Then should then see the "Iniciar sesión" button disabled


  Scenario: TC-0003 Validate "Requerido" message in the email field
    And I enter on "email" field
    Then should see the Requerido in the email field


  Scenario: TC-0004 Validate "Requerido" message in the password field
    And I enter on "password" field
    Then should see the Requerido in the password field


  Scenario: TC-0005 Validate message "El correo debe ser válido"
    And I enter email email.incorrect
    Then should see the El correo debe ser válido in the email field

  Scenario: TC-0006 Validate the message when the email is incorrect
    And I enter email email.incorrect@gmail.com
    And I enter password Test1234
    And I click on "Iniciar sesión" button
    Then should see the El e-mail o contraseña que ingresaste son incorrectos when the email is incorrect


  Scenario: TC-0007 Validate the message when the password is incorrect
    And I enter email pool.meza@gmail.com
    And I enter password incorrect
    And I click on "Iniciar sesión" button
    Then should see the El e-mail o contraseña que ingresaste son incorrectos when the password is incorrect


  Scenario: TC-0008 Validate redirecting link "Registrate"
    And I click on "Registrate" button
    Then should see the ¡Empecemos por tu nombre y tu mail! when entering the linktext


  Scenario: TC-0009 Validate that it redirects "¿Olvidaste tu contraseña?"
    And I click on "¿Olvidaste tu contraseña?" button
    Then should see the Olvidé mi contraseña when entering the linktext "¿Olvidaste tu contraseña?"

  Scenario: TC-0010 Validate "Iniciar sesión" button with Apple
    And I click on "Iniciar sesión con Apple" button
    Then should see the Apple ID when entering the linktext Login Apple


  Scenario: TC-0011 Validate "Iniciar sesión" button with Google
    And I click on "Iniciar sesión con Google" button
    Then should see the Iniciar sesión con Google when entering the linktext Login Google