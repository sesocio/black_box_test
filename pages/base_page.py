from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class BasePage:
    browser = None
    _instance = None
    __shared_state = dict()

    def __new__(cls, *args, **kwargs):
        """
        Singleton pattern: The idea is to restrict the object creation to only one instance.
        """
        if not cls._instance:
            cls._instance = object.__new__(cls)
        return cls._instance

    def __init__(self):
        """
        Borg pattern: The __dict__ holds all the attributes of the Borg class and __shared_state is a static variable.
        When instantiating a Borg class __init__ is called and it overrides the __dict__ with the static __shared_state,
        thus each class will have the same brain: __shared_state.
        If you modify variables, their values will be reflected in __shared_state.
        """
        self.__dict__ = self.__shared_state

    """
      Open browser and load page.
      :param url: The page.
      :param browser_name: The browser to run.
      :param executable_path: the path of the browser executable.
      :param implicitly_wait: The implicit time to open the browser and load the page.
    """

    def open(self, url, browser_name, executable_path, implicitly_wait):
        if browser_name.upper() == 'FIREFOX':
            self.browser = webdriver.Firefox(executable_path=executable_path.format("geckodriver"))
        elif browser_name.upper() == 'CHROME':
            self.browser = webdriver.Chrome(executable_path=executable_path.format("chromedriver"))
            self.browser.maximize_window()
        else:
            raise ValueError(u'The driver couldn\'t be created')
        self.browser.implicitly_wait(implicitly_wait)
        self.browser.get(url)

    def _get_element(self, locator):
        """
        Get element selected by locator.
        :param locator: The element selected.
        """
        element = self.browser.find_element(*locator)
        if not element:
            raise NoSuchElementException(u'There couldn\'t be found any elements with selector: {0}'.format(locator))
        return element

    def click(self, locator):
        """
          click element selected by locator.
          :param locator: The element selected.
          """
        self._get_element(locator).click()

    def send_keys(self, locator, field):
        """
          set field element selected by locator.
          :param field: A string for typing, or setting form fields
          :param locator: The element selected.
          """
        self._get_element(locator).send_keys(field)

    def is_displayed(self, locator):
        """
        Is displayed the element selected by the locator.
        :param locator: The element selected.
        """
        try:
            return self._get_element(locator).is_displayed()
        except NoSuchElementException:
            return False

    def waitForElement(self, locator, timeout=5, pollFrequency=0.5):
        """
        Wait For Element Clickable by the locator.
        :param locator: The element selected.
        :param timeout: Time implicit.
        :param pollFrequency: Frequency.
         """
        try:
            wait = WebDriverWait(self.browser, timeout, poll_frequency=pollFrequency,
                                 ignored_exceptions=[NoSuchElementException])
            return wait.until(EC.element_to_be_clickable(locator))
        except NoSuchElementException:
            return False

    def is_enable(self, locator):
        """
        Is enable the element selected by the locator.
        :param locator: The element selected.
        """
        try:
            return self._get_element(locator).is_enabled()
        except NoSuchElementException:
            return False

    def get_text(self, locator):
        """
        Get text from the element selected by the locator
        :param locator: The element selected.
        """
        return self._get_element(locator).get_attribute('innerHTML')

    def press_enter(self, locator):
        """
        Double click element selected by locator.
        :param locator: The element selected.
        """
        self._get_element(locator).send_keys(Keys.TAB)
