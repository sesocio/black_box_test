from selenium.webdriver.common.by import By

from pages.base_page import BasePage


class LoginPage(BasePage):
    _input_email = (By.NAME, 'email')
    _input_password = (By.NAME, 'password')
    _button_login = (By.XPATH, '//button[@type="submit"]')
    _avatar = (By.XPATH, '//*[@id="app"]/div[1]//button/span/div/div/div[3]')
    _button_ingresar = (By.XPATH, './/button/span[contains(text(),\'Ingresar\')]')
    _button_mi_cuenta = (By.ID, 'data-test-nav-0')
    _message_email = (By.XPATH, '//*[@id="app"]//form/div/div[3]/div[1]/div/div[2]/div/div/div')
    _message_password = (By.XPATH, '//*[@id="app"]//form/div/div[3]/div[2]/div/div[2]/div/div/div')
    _message_validation = (By.XPATH, '//span[contains(text(),\'El e-mail o contraseña que ingresaste son incorrec\')]')
    _button_registrate = (By.XPATH, '//a[contains(text(),"Registrate")]')
    _message_onboarding = (By.XPATH, '//div[2]/div/div/div/div/div/div/div/p')
    _button_olvidaste_contrasenia = (By.XPATH, '//form/div/div[3]/a/span')
    _message_olvidaste_contrasenia = (By.XPATH, '//div[4]/div/div/div/span')
    _button_login_google = (
        By.XPATH,
        '//div[@id="app"]/div[3]/div/div/div/div/div/div/div/div[2]/form/div/div[6]/div/div[2]/button/span/span')
    _message_login_google = (By.XPATH, '//*[@id="initialView"]/div[2]/div/div[1]/div/div[2]')

    _button_login_apple = (By.XPATH, '//div[6]/div/div/button')
    _message_login_apple = (By.XPATH, '//span[contains(text(),"Apple ID")]')

    def click_on_ingresar(self):
        self.click(self._button_ingresar)

    def set_email_input(self, email):
        self.send_keys(self._input_email, email)

    def set_password_input(self, password):
        self.send_keys(self._input_password, password)

    def click_on_login(self):
        self.click(self._button_login)

    def enter_on_email(self):
        self.press_enter(self._input_email)

    def enter_on_password(self):
        self.press_enter(self._input_password)

    def click_on_mi_cuenta(self):
        self.wait_for_element(self._button_mi_cuenta)
        self.click(self._button_mi_cuenta)

    def validate_login(self):
        return self.is_displayed(self._avatar)

    def validate_button_login(self):
        return self.is_enable(self._button_login)

    def validate_message_email(self):
        return self.get_text(self._message_email)

    def validate_message_password(self):
        return self.get_text(self._message_password)

    def validate_message_login(self):
        return self.get_text(self._message_validation)

    def click_on_registrate(self):
        self.click(self._button_registrate)

    def validate_message_registrate(self):
        return self.get_text(self._message_onboarding)

    def click_on_olvidaste_contrasenia(self):
        self.click(self._button_olvidaste_contrasenia)

    def validate_message_olvidaste_contrasenia(self):
        return self.get_text(self._message_olvidaste_contrasenia)

    def click_on_login_google(self):
        self.click(self._button_login_google)

    def validate_message_login_google(self):
        return self.get_text(self._message_login_google)

    def click_on_login_apple(self):
        self.click(self._button_login_apple)

    def validate_message_login_apple(self):
        return self.get_text(self._message_login_apple)
