from selenium.webdriver.common.by import By

from pages.base_page import BasePage


class DepositAmount(BasePage):
    _button_cargar_saldo = (
        By.XPATH,
        '//div[@id="app"]/div/header/div/div/div/div/div/div/div[2]/div/div[6]/div/div/div/div/div/div[3]/a/span')
    _button_ars = (By.ID, 'data-test-ARS')
    _input_amount = (By.ID, 'amount')
    _button_continuar = (By.ID, '(//button[@type="button"])[10]')
    _validate_message = (
        By.XPATH, '//div[@id="app"]/div/main/div/div/div[2]/div/div/div/div/div/div/div/div[2]/div/div/div')

    def click_on_cargar_saldo(self):
        self.click(self._button_cargar_saldo)

    def click_on_ars(self):
        self.click(self._button_ars)

    def set_amount_input(self, amount):
        self.send_keys(self._input_amount, amount)

    def click_on_continuar(self):
        self.click(self._button_continuar)

    def validate_message(self):
        return self.is_displayed(self._validate_message)


