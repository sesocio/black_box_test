import time


def after_scenario(context, scenario):
    """
    Run after each scenario is run.
    :param context: Place where you and behave can store information to share around.
    :param scenario: Illustrate a specific aspect of behavior of the application.
    """
    time.sleep(5)
    context.page.browser.quit()
