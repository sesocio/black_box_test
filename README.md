# _Automation in Python._
_The goal of the Python Automation Framework is to provide a set of tools 
and best practices for writing automation test scripts with respect to UI 
and services._
_The following sections will describe the tools and practices, as well as a 
guide to creating a test case._

### _Version._
_1.0._

### _How to download the repository?_
The repository can be cloned as follows:

#### _Pre-Conditions_
1. Have git installed (for windows)
2. Have access to the repository
3. To have configured the user and the mail in git

#### _HTTPS_
 1. Open the command line console **CMD** in the folder where you want to clone the repository.
 2. Copy the HTTPS of the repo to clone https://@bitbucket.org/sesocio/black_box_test.git

#### _SSH_
 1. Open the command line console **CMD** in the folder where you want to clone the repository.
 2. Copy the SSH of the repo to clone git@bitbucket.org:sesocio/black_box_test.git

### _Open the project with the IDE._

#### _Pre-Conditions_
1. Have PyCharm installed

#### _Steps_
1. Go to File and select open
2. Search for the project
3. Click on the OK button

### _How to generate my virtual environment within the project._
1. Go to File and select settings
2. Go to project:automation
3. Go to python interpreter
4. Go to settings
5. Select show all
6. Click on the + button
7. Select virtualenv environment
8. Apply changes

### _Install dependencies._
Create a file called requirements.txt where you must write the dependencies 
to be used to install them. 

### _Directory structure._
![](image/structure.png)


### _Pattern design page object._
The Page Object Model is the solution that was found to the problem that 
functions are repeated and actions have to be done several times for 
different tests. It is very cumbersome to have to change each implementation 
when something changed in the test, so what this approach does is to model 
each page of our application only once and call the actions on it as many 
times as we need, from a single source.


### _Libraries to be used in the project._
* [Setuptools](https://setuptools.pypa.io/en/latest/)
* [Pip](https://pypi.org/project/pip/)
* [Behave](https://behave.readthedocs.io/en/stable/#)
* [Pep8](https://ellibrodepython.com/python-pep8)
* [Hamcrest](http://hamcrest.org/)

### _How to run a test case._
To run the test cases you must be in the root folder where the framework 
is installed. where the framework is installed.

#### _Virtual environment_
    .\venv\Script\active

#### _Run script_
    behave features/login.feature

### _About behave._
* _[Gherkin Language]_ - _Gherkin language support._
* _[Pdf Documentation]_ - _Benno Rice, Richard Jones and Jens Engel (Release 1.2.6.dev0)._

### _How to use debug._

### _How to use Allure Report._
Before we must have installed the Allure server, for that we will go to the 
following page https://docs.qameta.io/allure/#_get_started and there we will 
follow the steps depending on the system that we have.

#### _Run script with Allure Report_
We execute the following command to run the tests and generate a folder 
containing the json of the runs.

    behave -f allure_behave.formatter:AllureFormatter -o reports/ features/login.feature

#### _View Allure Report_
Generate a folder, which has an index.html file that allows us to view the 
generated report.

    allure generate .\reports\

![](image/allure.png)

#### _Debug configurations_
Generate a file, which has as name run.py where it contains the run main.


![](image/run_py.png)

Run/debug configurations

1. Select Run | Edit Configurations from the main menu.

2. In the Run/Debug Configuration dialog, click Icons general add on the toolbar. The list shows the run/debug configuration templates.

3. Specify the run/debug configuration name in the Name field. This name will be shown in the list of the available run/debug configurations.

4. Set the run/debug configuration parameters.

5. Apply the changes and close the dialog.

![](image/config_debug.png)

Execution of debugging

Mark the endpoint and click debug

![](image/debug.png)

### _How to commit the test case._
- _Create local branch according to the story to automate (respect branch name format)._

        (venv) user@nb automation % git branch tc-813-Profile-FAQs

- _Ready to be created correctly._
  
        (venv) user@nb automation % git branch --list 
        tc-813-Profile-FAQs
        * master

- _Change to branch created._
  
        (venv) user@nb automation %  git checkout tc-813-Profile-FAQs

- _Commands to commit, push on a branch in remote repository._

        (venv) user@nb automation % git status
        (venv) user@nb automation % git add .
        (venv) user@nb automation % git status
        (venv) user@nb automation % git commit -m "tc-813-Profile-FAQs"
        (venv) user@nb automation % git log -1
        (venv) user@nb automation % git push --set-upstream origin tc-813-Profile-FAQs

- _I check that it has been created correctly in the repo via bitbucket or by console._
  
        (venv) user@nb automation % git branch --all
        * tc-813-Profile-FAQs
          master
          remotes/origin/HEAD -> origin/master
          remotes/origin/develop
          remotes/origin/tc-813-Profile-FAQs
          remotes/origin/master

- _Code review by a colleague._

- _Change from working branch to master._

        (venv) user@nb automation % git checkout master        

- _I save the changes made from the job branch to the master._

        (venv) user@nb automation % git merge --no-ff tc-813-Profile-FAQs
        
- _Push changes from master to remote repository._

        (venv) user@nb automation % git push

- _Delete the local branch created for the test case implementation._

        (venv) user@nb automation % git branch -d tc-813-Profile-FAQs 

- _Remove the remote branch._

        (venv) user@nb automation % git push origin :tc-813-Profile-FAQs 

### _License._
_Copyright (c) 2021 SESOCIO._
